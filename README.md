django-tutorial
===

This is the tutorial project from [https://docs.djangoproject.com/en/1.5/](https://docs.djangoproject.com/en/1.5/). I keep it here as a reference.

Running
===
Migrate the database: 

`python manage.py syncdb`

Start the server: 

`python manage.py runserver`

Testing
===
Run tests: 

`python manage.py test polls`